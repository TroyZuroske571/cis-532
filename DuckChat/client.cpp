/*******************************************************************************
File name:  client.cpp
Author:     Troy Zuroske
Date:       11/5/2015
Class:      CS532
Assignment: DuckChat
Purpose:    This is the implementation file for each for the client side of
		    DuckChat.
*******************************************************************************/

#include "duckchat.h"
#include "raw.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <set>
#include <string>

using namespace std;

#define true 1
#define false 0
#define BUFFSIZE 1024

const char commonChannel[CHANNEL_MAX] = "Common";
char activeChannel[CHANNEL_MAX];
char inputBuffer[SAY_MAX + 1];
char *bufPosition= inputBuffer;
const int backspace = 127;
set<string> sChanSet;
int sockUDP = 0;
struct addrinfo * serverInfo = NULL;
struct sockaddr_in * serverAddress;
socklen_t serverAddressLen = sizeof(serverAddress);
const int KEEP_ALIVE_FREQ = 60;
bool bTimeAlive = false;

int socket_set (char*, char*);
int sendMessage (struct request*, int);
int handleSay(const char * msg);
void nextInputArrow ();
int getCommand(char *);
void clearTerminal ();
char* newInput();
int userLogin (const char*);
int joinChat (const char*);
int leaveChannel (const char*);
int listChannels ();
int initSwitch (const char*);
int serverResponse (struct text*);
int whoIsOnThisChannel (const char*);
int errorHandler(const char *);
int errorReceived(struct text_error *);
int listReceived(struct text_list *);
int whoReceived(struct text_who *);
int sayReceived(struct text_say*);
int exitChat ();
int sendKeepAlive();
void timerExpired(int signum);

/*******************************************************************************
 Function:    main

 Description: Initiates program execution.

 Parameters:  argv[1] - Address to use
 	 	 	  argv[2] - Port to use
 	 	 	  argv[3] - Username to login with

 Returned:    Success if no error, else -1
*******************************************************************************/
int main (int argc, char *argv[])
{
	char * address;
	char * port;
	char * username;
	char * input;
	int commandFromUser;
	fd_set FDSset;
	struct text * pText;
	int bytes;

	if (argc!= 4)
	{
		printf("Usage: server_socket server_port username\n");
	    exit (1);
	}

	raw_mode();
	memset(activeChannel, '\0', CHANNEL_MAX);

	address = argv[1];
	port = argv[2];
	username = argv[3];

	if(socket_set(address, port) != true)
	{
		cooked_mode();
		return 1;
	}

	if(userLogin(username) != true)
	{
		cooked_mode();
		return 1;
	}
	commandFromUser = true;

	signal(SIGALRM, timerExpired);
	alarm(KEEP_ALIVE_FREQ);
	nextInputArrow();

	do
	{
		FD_ZERO(&FDSset);
		FD_SET(0, &FDSset);
		FD_SET(sockUDP, &FDSset);
		select((sockUDP + 1), &FDSset, NULL, NULL, NULL);

		if(FD_ISSET(0, &FDSset))
		{
			input = new char();
			while (!bTimeAlive)
			{
				input = newInput();
				break;
			}
			if (bTimeAlive)
			{
				sendKeepAlive ();
				bTimeAlive = false;
			}
			else if(input != NULL)
			{
				commandFromUser = getCommand(input);

				if(commandFromUser != -1)
				{
					nextInputArrow();
				}
			}

		}
		else if (FD_ISSET(sockUDP, &FDSset))
		{
			pText = (struct text *) malloc(sizeof(struct text) + BUFFSIZE);
			bytes = 0;

			if((bytes = recvfrom(sockUDP, pText, BUFFSIZE, 0,
					             serverInfo->ai_addr,
								 &serverInfo->ai_addrlen)) > 0)
			{
				clearTerminal();
				serverResponse(pText);
				free(pText);
			}
		}
	} while (commandFromUser != -1);

	freeaddrinfo(serverInfo);
	cooked_mode();
	return EXIT_SUCCESS;
}

/*******************************************************************************
 Function:    nextInputArrow

 Description: Prompt user for next input by printing >.

 Parameters:  None

 Returned:    None
*******************************************************************************/
void nextInputArrow()
{
	printf("> ");
	fflush(stdout);
}

/*******************************************************************************
 Function:    clearTerminal

 Description: Clear the buffer.

 Parameters:  None

 Returned:    None
*******************************************************************************/
void clearTerminal()
{
	printf("\b\b");
	fflush(stdout);
}

/*******************************************************************************
 Function:    errorHandler

 Description: Print the error that occured to the screen.

 Parameters:  errorOccured - the error to be printed

 Returned:    true
*******************************************************************************/
int errorHandler(const char * errorOccured)
{
	fprintf(stderr, "%s", errorOccured);
	fflush(stderr);
	return true;
}

/*******************************************************************************
 Function:    handleSay

 Description: When the user types/enters text at the prompt and hits 'Enter',
 	 	 	  the text is sent to the server using handleSay which calls
 	 	 	  sendMessage.

 Parameters:  newMessage - the user's new message to be sent

 Returned:    true if message is sent successfully else false because of error
*******************************************************************************/
int handleSay(const char * newMessage)
{
	int result;
	struct request_say * pReq;
	if(newMessage == NULL || strcmp(activeChannel, "") == 0)
	{
		result = false;
		return result;
	}
	pReq  = (struct request_say *) malloc(sizeof(struct request_say));
	pReq -> req_type = REQ_SAY;
	strncpy(pReq->req_channel, activeChannel, CHANNEL_MAX);

	strncpy (pReq->req_text, newMessage, SAY_MAX);

	result = sendMessage((struct request *) pReq, sizeof(struct request_say));

	free(pReq);
	return result;
}

/*******************************************************************************
 Function:    exitChat

 Description: The user request to logout so a logout request is sent to server.

 Parameters:  None

 Returned:    true if logout is sent successfully else false because of error
*******************************************************************************/
int exitChat()
{
	struct request_logout * pReq;
	pReq = (struct request_logout *) malloc(sizeof(struct request_logout));
	pReq -> req_type = REQ_LOGOUT;

	sendMessage((struct request *) pReq, sizeof(struct request_logout));

	free(pReq);
	return -1;
}

/*******************************************************************************
 Function:    joinChat

 Description: User wishes to join a channel, join request is sent, channel is
 	 	 	  created if it does not exist yet. User is added to the channel set

 Parameters:  channel - The channel the user wants to join

 Returned:    true if join is sent successfully else false because of error
*******************************************************************************/
int joinChat(const char * channel)
{
	struct request_join * pReq;
	int result;
	if(channel != NULL)
	{
		pReq = (struct request_join*) malloc(sizeof(struct request_join));
		pReq-> req_type = REQ_JOIN;
		strncpy(pReq->req_channel, channel, CHANNEL_MAX);

		result = sendMessage((struct request *) pReq,
				sizeof(struct request_join));

		if(result == true)
		{
			string chan = channel;
			sChanSet.insert(chan);
		}
		strncpy(activeChannel, channel, CHANNEL_MAX);

		free(pReq);
		return result;
	}
	else
	{
		result = false;
		errorHandler("Can not join channel\n");
		return result;
	}
}

/*******************************************************************************
 Function:    leaveChat

 Description: User wishes to leave a channel, leave request is sent, User is
 	 	 	  removed from the channel set

 Parameters:  channelToExit - The channel the user wants to leave

 Returned:    true if leave is sent successfully and user is removed else false
              because of error
*******************************************************************************/
int leaveChannel (const char * channelToExit)
{
	int result;
	struct request_leave *pReq;
	if(channelToExit == NULL)
	{
		errorHandler("/leave <channel>\n");
		result = false;
		return result;
	}

	pReq = (struct request_leave*) malloc(sizeof(struct request_leave));
	pReq -> req_type = REQ_LEAVE;
	strncpy(pReq -> req_channel, channelToExit, CHANNEL_MAX);

	result = sendMessage((struct request *) pReq,
						  sizeof(struct request_leave));

	if(result == true)
	{
		string sChannel = channelToExit;
		sChanSet.erase(sChannel);
	}

	free(pReq);
	return result;
}

/*******************************************************************************
 Function:    listChannels

 Description: User wishes to list all channels available

 Parameters:  NONE

 Returned:    true if list is sent successfully else false because of error
*******************************************************************************/
int listChannels ()
{
	int result;
	struct request_list * pReq;

	pReq = (struct request_list *) malloc(sizeof(struct request_list));
	pReq -> req_type = REQ_LIST;

    result = sendMessage((struct request *) pReq, sizeof(struct request_list));

	free(pReq);
	return result;
}

/*******************************************************************************
 Function:    whoIsOnThisChannel

 Description: User wishes to see who is on a certain channel

 Parameters:  channel - The channel the user wants to see who's on

 Returned:    true if who is sent successfully and user is shown else false
              because of error
*******************************************************************************/
int whoIsOnThisChannel(const char * channel)
{
	int result;
	struct request_who * pReq;
	if(channel == NULL)
	{
		errorHandler("/who <channel>\n");
		result = false;
		return result;
	}

	pReq = (struct request_who *) malloc(sizeof(struct request_who));
	pReq -> req_type = REQ_WHO;
	strncpy(pReq -> req_channel, channel, CHANNEL_MAX);

	result = sendMessage((struct request *) pReq,
			                  sizeof(struct request_who));
	free(pReq);
	return result;
}

/*******************************************************************************
 Function:    getCommand

 Description: Get the command the user has inputed.

 Parameters:  input - The user's input.

 Returned:    true if leave if command is found else false because of error
*******************************************************************************/
int getCommand(char *input)
{
	const char *pNEWLINE = " \n";
	char *pTok;
	char *pCheckCommand;
	int result;

	pTok = (char*) malloc(sizeof(char)*(strlen(input) + 1));
	strcpy(pTok, input);
	pCheckCommand = strtok(pTok, pNEWLINE);
	if (pCheckCommand == NULL)
	{
		result = false;
		return result;
	}

	result = false;
	if (strcmp(pCheckCommand, "/list") == 0)
	{
		result = listChannels();
	}
	else if (strcmp(pCheckCommand, "/who") == 0)
	{
		char * channel = strtok(NULL, pNEWLINE);
		result = whoIsOnThisChannel(channel);
	}
	else if (strcmp(pCheckCommand, "/switch") == 0)
	{
		char * channel = strtok(NULL, pNEWLINE);
		result = initSwitch(channel);
	}
	else if (strcmp(pCheckCommand, "/exit") == 0)
	{
		result = exitChat();
	}
	else if (strcmp(pCheckCommand, "/join") == 0)
	{
		char * channel = strtok(NULL, pNEWLINE);
		result = joinChat(channel);
	}
	else if (strcmp(pCheckCommand, "/leave") == 0)
	{
		char * channel = strtok(NULL, pNEWLINE);
		result = leaveChannel(channel);
	}
	else
	{
		result = handleSay(input);
	}

	free(pTok);
	return result;
}

/*******************************************************************************
 Function:    initSwitch

 Description: User wishes to switch to a new or current channel

 Parameters:  channel - The channel the user wishes to switch to

 Returned:    true if switch is successful else false because of error
*******************************************************************************/
int initSwitch(const char * channel)
{
	int result;
	set<string>::iterator iterateChannels;

	if((iterateChannels = sChanSet.find(channel)) != sChanSet.end())
	{
		strncpy(activeChannel, iterateChannels-> c_str(), CHANNEL_MAX);
		result = true;
		return result;
	}

	result = false;
	printf("You have not subscribed to channel %s \n", channel);
	return result;
}

/*******************************************************************************
 Function:    serverResonse

 Description: The servers response to display to the user given the command

 Parameters:  reply - The reply the server has sent

 Returned:    true if server sends valid response else false because of error
*******************************************************************************/
int serverResponse (struct text* reply)
{
	switch((int)reply->txt_type)
	{
		case TXT_ERROR:
			return errorReceived((struct text_error *) reply);
		case TXT_LIST:
			return listReceived((struct text_list *) reply);
		case TXT_WHO:
			return whoReceived((struct text_who *) reply);
		case TXT_SAY:
			return sayReceived((struct text_say *) reply);
		default:
			errorHandler("Unknown response type received\n");
			return false;
	}
}

/*******************************************************************************
 Function:    errorReceived

 Description: The error handler to display errors to the user

 Parameters:  error - The error that has occurred

 Returned:    true no matter what
*******************************************************************************/
int errorReceived(struct text_error * error)
{
	errorHandler(error->txt_error);
	printf("\n");
	nextInputArrow();
	return true;
}

/*******************************************************************************
 Function:    listReceived

 Description: A list has been received from the server

 Parameters:  list - A text_list struct containing the list of channels

 Returned:    true if list is successful else false because of error
*******************************************************************************/
int listReceived(struct text_list * list)
{
	int i;
	int numChannels;

	numChannels = ((int)list -> txt_nchannels);

	if((bufPosition - inputBuffer) > 0)
	{
		printf("\n");
	}

	printf("Existing Channels: \n");

	for(i = 0; i < numChannels; i++)
	{
		printf("  %s\n", list-> txt_channels[i].ch_channel);
	}

	nextInputArrow();

	if((bufPosition - inputBuffer) > 0)
	{
		char *cInput;
		for(cInput = inputBuffer; cInput < bufPosition; cInput++)
		{
			printf("%c", *cInput);
		}
	}

	fflush(stdout);
	return true;
}

/*******************************************************************************
 Function:    whoReceived

 Description: Server has sent a list of who's on the requested channel

 Parameters:  whosOnline - a text_who struct containing a list of users on the
 	 	 	 	 	 	   requested channel

 Returned:    true if whoReceived is successful else false because of error
*******************************************************************************/
int whoReceived(struct text_who * whosOnline)
{
	int numUsers = ((int)whosOnline -> txt_nusernames);

	if((bufPosition - inputBuffer) > 0) {
		printf("\n");
	}

	printf("Users on channel %s:\n", whosOnline->txt_channel);

	int i;
	for(i = 0; i < numUsers; i++) {
		printf("  %s\n", whosOnline -> txt_users[i].us_username);
	}

	nextInputArrow();

	if((bufPosition - inputBuffer) > 0)
	{
		char *cInput;
		for(cInput = inputBuffer; cInput < bufPosition; cInput++)
		{
			printf("%c", *cInput);
		}
	}

	fflush(stdout);
	return true;
}

/*******************************************************************************
 Function:    sayReceived

 Description: A message has been received from the server

 Parameters:  newMesage - The message a user has sent being received

 Returned:    true if message is received else false because of error
*******************************************************************************/
int sayReceived(struct text_say* newMessage)
{
	if((bufPosition - inputBuffer) > 0) {
		printf("\n");
	}

	printf("[%s][%s]: %s\n", newMessage->txt_channel, newMessage->txt_username,
			newMessage->txt_text);

	nextInputArrow();

	if((bufPosition - inputBuffer) > 0)
	{
		char *cInput;
		for(cInput = inputBuffer; cInput < bufPosition; cInput++)
		{
			printf("%c", *cInput);
		}
	}

	fflush(stdout);
	return true;
}

/*******************************************************************************
 Function:    newInput

 Description: Get user's input one character at a time

 Parameters:  None

 Returned:    a char* containing user's input from command line
*******************************************************************************/
char* newInput()
{
	char cInput;
	cInput = getchar();

	if (((int)cInput) == backspace)
	{
		if(bufPosition > inputBuffer)
		{
			--bufPosition;
			printf("\b");
			fflush(stdout);
		}
	}
	else if(cInput == '\n')
	{
		*bufPosition++ = '\0';
		bufPosition = inputBuffer;
		printf("\n");
		fflush(stdout);
		return inputBuffer;

	}
	else if (bufPosition != inputBuffer + SAY_MAX)
	{
		*bufPosition++ = cInput;
		printf("%c", cInput);
		fflush(stdout);
		return NULL;
	}
	return NULL;
}

/*******************************************************************************
 Function:    sock_set

 Description: Set up the socket using UDP

 Parameters:  address - The address we are connecting to
 	 	 	  port    - The port number we are connecting on

 Returned:    true - if socket is setup successfully, false - if error occurred
*******************************************************************************/
int socket_set (char* address, char* port)
{
	int result = 0;
	struct addrinfo addressInfo;
	struct addrinfo *pAddress;
	memset(&addressInfo, 0, sizeof pAddress);
	addressInfo.ai_family = AF_INET;
	addressInfo.ai_socktype = SOCK_DGRAM;

	if((result = getaddrinfo(address, port, &addressInfo, &serverInfo)) != 0)
	{
		fprintf(stderr, "Address info: %s\n",gai_strerror(result));
		return false;
	}

	for(pAddress = serverInfo; pAddress != NULL; pAddress = pAddress -> ai_next)
	{
		sockUDP = socket(pAddress -> ai_family, pAddress -> ai_socktype,
				         pAddress -> ai_protocol);

		if (sockUDP == -1)
		{
			perror("socket: ");
			continue;
		}
		break;
	}

	serverInfo = pAddress;

	return(pAddress != NULL);
}

/*******************************************************************************
 Function:    userLogin

 Description: New user wishes to login to channel Common

 Parameters:  username - The user's name they wish to login with

 Returned:    true if login is successful else false because of error
*******************************************************************************/
int userLogin(const char * username)
{
	struct request_login * pReq;
	int result;

	pReq= (struct request_login*) malloc(sizeof(struct request_login));
	pReq-> req_type = htonl(REQ_LOGIN);
	strncpy(pReq-> req_username, username, USERNAME_MAX);

	result = sendMessage((struct request*) pReq, sizeof(struct request_login));

	free(pReq);

	return result && joinChat(commonChannel);;
}

int sendMessage(struct request * pReq, int length)
{
	int result = true;
	result = sendto(sockUDP, pReq, length, 0, serverInfo-> ai_addr,
			        serverInfo-> ai_addrlen);
	if(result == -1)
	{
		result = false;
		return result;
	}
	alarm(KEEP_ALIVE_FREQ);
	return true;
}

/*******************************************************************************
 Function:    timerExpired

 Description: A timer to make sure a user has sent something every minute

 Parameters:  signum - the signal

 Returned:    None
*******************************************************************************/
void timerExpired(int signum)
{
	bTimeAlive = true;
	signal(SIGALRM, timerExpired);
	alarm(KEEP_ALIVE_FREQ);
}

/*******************************************************************************
 Function:    sendKeepAlive

 Description: User has not sent anything within the last minute so will
 	 	 	  automatically keep alive message to server

 Parameters:  None

 Returned:    true if keep alive sent successful else false because of error
*******************************************************************************/
int sendKeepAlive()
{
    int result;
	struct request_keep_alive * pReq;

	pReq = (struct request_keep_alive *) malloc(sizeof(struct request_keep_alive));
	pReq -> req_type = REQ_KEEP_ALIVE;

	result = sendMessage((struct request *) pReq, sizeof(struct request_list));

	free(pReq);
	return result;
}


