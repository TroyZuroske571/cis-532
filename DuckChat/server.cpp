/*******************************************************************************
File name:  server.cpp
Author:     Troy Zuroske
Date:       12/3/2015
Class:      CS532
Assignment: DuckChat part 2
Purpose:    This is the implementation file for each for the server side of
		    DuckChat.
*******************************************************************************/

#include "duckchat.h"
#include <iostream>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <map>
#include <set>
#include <string>
#include <utility>
#include <vector>
#include <signal.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <netdb.h>
#include <string.h>
#include <time.h>

using namespace std;

#define BUFFSIZE 1024
#define true 1
#define false 0

string strHost;
string strIPHost;
struct addrinfo *serverInfo = NULL;
struct sockaddr_storage mostRecentUser;
socklen_t recentUserSize= sizeof(mostRecentUser);
int sockUDP = 0;
int maxUDP= 0;
const int arguments = 3;
const int loopArgs = 2;
char inputBuffer[BUFFSIZE];
map<string, long> mUserAliveCheck;
multimap<string, string> mChan2User;
multimap<string, string> mChan2Server;
map<string, string> mAddress2User;
map<string, string> mUser2Address;
struct timeval begin, now;
vector<pair<string,long> > vAdressServer;
set<string> sChanSet;
set<long long> sIDList;
multimap<string, string> mUser2Chan;
const int JOIN_TIME = 60;
bool evenMin;
int socket_set (char*, char*);
int initSwitch (struct request*, int);
int sendMessage (const struct sockaddr*, size_t, struct text*, int);
int sendLast(struct text*, int);
struct sockaddr *sStringAddress(string);
void timesUp(int);
int logoutReceived(struct request_logout*);
int joinReceived(struct request_join*);
int leaveReceived(struct request_leave*);
int loginReceived(struct request_login*);
int listReceived(struct request_list*);
int whoReceived(struct request_who*);
long long IDgenerator();
int sayReceived(struct request_say*, long long, char*);
int s2sJoinReceived(request_s2s_join*);
int s2sLeaveReceived(request_s2s_leave*);
int s2sSayReceived(request_s2s_say*);
int serv2Chan (string, const char *);
string strAddressString(const struct sockaddr_in*);
int addNewUser(const char*);
int joinToChannel(const char*, const char*);
int eraseChanFromSer(string, char *);
int removeRecentUser();
int eraseFromChan(const char*, const char*);
int s2sSayNewID(char*, char*, char*);
int s2sSay(struct request_s2s_say*);
int kickUserOff (const char*, const char*);
string strAddressUser(const struct sockaddr_in*);
int say(const char*, const char*, const char*, const struct sockaddr*);
int whoIsOnThisChannel(const char*);
int listChannels();
int s2sJoinChan(const char*);
int s2sLeaveChan(const char*);
bool chanDuplicatedCheck (string, const char *);
int s2sRelay(struct request*, int, string);
int s2sSend(const struct sockaddr*, size_t, struct request*, int);
int s2sSendLast(struct request*, int);
int keepAliveReceived (struct request_keep_alive*);
void sendOutJoins(int);


/*******************************************************************************
 Function:    main

 Description: Initiates program execution.

 Parameters:  argv[1] - Address to use
 	 	 	  argv[2] - Port to use

 Returned:    Success if no error, else -1
*******************************************************************************/
int main (int argc, char *argv[])
{
	int bytes;

	evenMin = false;
	signal(SIGALRM, sendOutJoins);
	alarm (JOIN_TIME);

	if (argc % 2 == 0 || argc <= 1)
	{
		printf("Usage: ./server domain_name port_num \n");
		return 1;
	}

	char * input = (char*) malloc(sizeof(char) * BUFFSIZE);

	socket_set(argv[1], argv[2]);
	maxUDP = sockUDP;
	sprintf(input, "%s:%s", argv[1], argv[2]);
	strHost = input;
	struct sockaddr_in * sLocalAddress = (struct sockaddr_in*) sStringAddress(strHost);
	sLocalAddress-> sin_port= ntohs(sLocalAddress-> sin_port);
	strIPHost = strAddressString((struct sockaddr_in*)sLocalAddress);

	for (int i = arguments; i < argc; i += loopArgs)
	{
		string IPaddr;
		string inputAddressTwo;

		char * inputTwo = (char*) malloc(sizeof(char) * BUFFSIZE);
		sprintf(inputTwo, "%s:%s", argv[i], argv[i+1]);
		IPaddr = inputTwo;
		struct sockaddr_in * inputAddress = (struct sockaddr_in*) sStringAddress(IPaddr);
		inputAddress-> sin_port= ntohs(inputAddress-> sin_port);
		inputAddressTwo = strAddressString((struct sockaddr_in*)inputAddress);
		gettimeofday(&begin , NULL);
		vAdressServer.push_back(pair<string,long>(inputAddressTwo, begin.tv_sec));
	}

	int j = vAdressServer.size();
	free(input);
	bytes = 0;

	while (true)
	{
		struct request *pReq= (struct request*) malloc(sizeof (struct request)
								+ BUFFSIZE);
		if ((bytes = recvfrom(sockUDP, pReq, BUFFSIZE, 0,
					(struct sockaddr*)&mostRecentUser, &recentUserSize)) > 0)
		{
			initSwitch(pReq, bytes);
		}
		free(pReq);
	}

	freeaddrinfo(serverInfo);

	return 0;
}

/*******************************************************************************
 Function:    strAddressUser

 Description: Gets user from their address.

 Parameters:  addr - the address to look up in the map

 Returned:    The username
*******************************************************************************/
string strAddressUser(const struct sockaddr_in* addr)
{
	string mUsername;
	string sAddr= strAddressString(addr);
	mUsername = mAddress2User[sAddr];
	return mUsername;
}

/*******************************************************************************
 Function:    strAddressString

 Description: Get ip address of user

 Parameters:  addr - the address to look up

 Returned:    the ip address
*******************************************************************************/
string strAddressString(const struct sockaddr_in* addr)
{
	char *ad = (char*) malloc(sizeof(char)*BUFFSIZE);
	string ip;
	char port[6];

	inet_ntop(AF_INET, &(addr->sin_addr), ad, BUFFSIZE);

	ip = ad;
	free(ad);

	snprintf(port, 6, "%hu", addr->sin_port);
	ip = (ip + ":" + port);

	return ip;
}

/*******************************************************************************
 Function:    sStringAddress

 Description: Gets socket address for the specified socket.

 Parameters:  sAdr - the address to look up

 Returned:    The socket address struct
*******************************************************************************/
struct sockaddr *sStringAddress(string straddr)
{
	struct sockaddr_in * sa= (struct sockaddr_in*) malloc(sizeof(struct sockaddr_in));

	char *tok= (char*) malloc(sizeof(char)*BUFFSIZE);
	strcpy(tok, straddr.c_str());

	char *ip= strtok(tok, ":");
	char *port= strtok(NULL, ":");
	struct addrinfo addr, *res;
	memset(&addr, 0, sizeof addr);
	addr.ai_family= AF_INET;
	addr.ai_socktype= SOCK_DGRAM;
	int result= getaddrinfo(ip, port, &addr, &res);

	return res->ai_addr;
}

/*******************************************************************************
 Function:    timesUp

 Description: Kicks user off if they have not sent anything for two minutes

 Parameters:  time - the time

 Returned:    None
*******************************************************************************/
void timesUp(int clock)
{
	set<string>::const_iterator iter;
	string channel;
	struct request_s2s_join *pRequest;

	if (evenMin == true)
	{
		if(!sChanSet.empty())
		{
			for (iter = sChanSet.begin(); iter !=sChanSet.end(); ++iter)
			{
				const char * chan = iter->c_str();
				channel = *iter;
				pRequest = (struct request_s2s_join*) malloc(sizeof(struct request_s2s_join));
				pRequest->req_type = REQ_S2S_JOIN;
				int result;
				char * user;

				map<string,long>::iterator itertwo;

				for (itertwo = mUserAliveCheck.begin(); itertwo != mUserAliveCheck.end(); itertwo++)
				{
					const char * user = itertwo->first.c_str();
					gettimeofday(&now , NULL);
					result = (now.tv_sec - itertwo->second);
					if (user != NULL)
					{
						if (result > 120)
						{
							kickUserOff(user, chan);
							printf ("%s server: forcibly removing user %s\n", strIPHost.c_str(), user);
						}
					}
				}
			}
		}
		int j = vAdressServer.size();
		vector<pair<string,long> >::iterator it;
		for(it = vAdressServer.begin(); it != vAdressServer.end() && !(vAdressServer.empty());)
		{
			int joinTime;
			gettimeofday(&now , NULL);
			joinTime = (now.tv_sec - it->second);
			if (joinTime > 120)
			{
				map<string,string>::iterator chanIter;
				for (chanIter = mChan2Server.begin(); chanIter != mChan2Server.end(); chanIter++)
				{
					if (it->first == chanIter->second)
					{
						printf ("%s failed to send join within 2 min \n", chanIter->second.c_str());
						s2sLeaveChan(chanIter->first.c_str());
					}
				}
				it = vAdressServer.erase(it);
			}
			else
			{
				++it;
			}
		}

		evenMin = false;
	}
	else
	{
		evenMin = true;
	}

	signal(SIGALRM, sendOutJoins);
	alarm(JOIN_TIME);
}

/*******************************************************************************
 Function:    IDgenerator

 Description: Generates unique ID

 Parameters:  None

 Returned:    The ID
*******************************************************************************/
long long IDgenerator()
{
	long long newID= 0LL;
	int randomNumber;
	int sizeOfRandom = 8;
	int valReturned;

	randomNumber = open("/dev/urandom", O_RDONLY);

	if (randomNumber == -1)
	{
		return 0;
	}

	valReturned= read(randomNumber, &newID, sizeOfRandom);

	if (valReturned == 0)
	{
		return 0;
	}
	return newID;
}

/*******************************************************************************
 Function:    sock_set

 Description: Set up the socket using UDP

 Parameters:  address - The address we are connecting to
 	 	 	  port    - The port number we are connecting on

 Returned:    true - if socket is setup successfully, false - if error occurred
*******************************************************************************/
int socket_set(char *addr, char *port) {

	int result= 0;

	// Setup hints
	struct addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family= AF_INET;
	hints.ai_socktype= SOCK_DGRAM;
	hints.ai_flags= AI_PASSIVE;

	// Get address info struct
	if ((result= getaddrinfo(addr, port, &hints, &serverInfo)) != 0)
	{
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(result));
		return false;
	}

	// Create UDP socket
	sockUDP= socket(serverInfo->ai_family, serverInfo->ai_socktype, serverInfo->ai_protocol);

	if (sockUDP == -1) {
	perror("socket failed \n");
		return false;
	}

	//bind
	if ((bind(sockUDP, serverInfo->ai_addr, serverInfo->ai_addrlen)) == -1) {
		perror("bind failed \n");
		return false;
	}
	return true;
}

/*******************************************************************************
 Function:    initSwitch

 Description: Gets the request and matches it the correct command

 Parameters:  pReq - The request coming in
 	 	 	  len  - The length of the request

 Returned:    true - if socket command is successful, false - if error occurred
*******************************************************************************/
int initSwitch(struct request* pReq, int len)
{
	int result = false;
	int requestType;

	if (pReq->req_type != REQ_S2S_LEAVE &&
		pReq->req_type != REQ_S2S_SAY &&
		pReq->req_type != REQ_S2S_JOIN)
	{
		if (pReq->req_type != REQ_LOGIN &&
			(strAddressUser((struct sockaddr_in*)&mostRecentUser) == "") )
		{
			printf("Received request type %d from unknown user; ignoring \n",
					pReq->req_type);

			return result;
		}
	}

	requestType = pReq->req_type;
	switch(requestType)
	{
		case REQ_S2S_LEAVE:
			if (sizeof(struct request_s2s_leave) != len)
			{
				printf("Size of S2S_Leave not correct\n");
				result= false;
				break;
			}
			result= s2sLeaveReceived((struct request_s2s_leave*) pReq);
			break;
		case REQ_S2S_JOIN:
			if (sizeof(struct request_s2s_join) != len)
			{
				printf("Size of S2S_Join not correct\n ");

				result= false;
				break;
			}
			result = s2sJoinReceived((struct request_s2s_join*)pReq);
			break;
		case REQ_S2S_SAY:
			if (sizeof(struct request_s2s_say) != len)
			{
				printf("Size of S2S_Say not correct\n");
				result= false;
				break;
			}
			result= s2sSayReceived((struct request_s2s_say*)pReq);
			break;
		case REQ_LOGIN:
			if (sizeof(struct request_login) != len)
			{
				printf("Size of login request not correct \n");
				result = false;
				break;
			}
			result= loginReceived((struct request_login*)pReq);
			break;
		case REQ_LOGOUT:
			if (sizeof(struct request_logout) != len)
			{
				printf("Size of logout request not correct \n");
				result = false;
				break;
			}
			result = logoutReceived((struct request_logout*)pReq);
			break;
		case REQ_LIST:
			if (sizeof(struct request_list) != len)
			{
				printf("Size of list request not correct\n");
				result= false;
				break;
			}
			result= listReceived((struct request_list*)pReq);
			break;
		case REQ_WHO:
			if (sizeof(struct request_who) != len)
			{
				printf("Size of who request not correct\n");
				result= false;
				break;
			}
			result= whoReceived((struct request_who*)pReq);
			break;
		case REQ_LEAVE:
			if (sizeof(struct request_leave) != len)
			{
				printf("Size of leave request not correct\n");
				result= false;
				break;
			}
			result = leaveReceived((struct request_leave*)pReq );
			break;
		case REQ_SAY:
			if (sizeof(struct request_say) != len)
			{
				printf("Size of say request not correct \n");
				result= false;
				break;
			}
			result= sayReceived((struct request_say*) pReq, 0, NULL);
			break;
		case REQ_JOIN:
			if (sizeof(struct request_join) != len)
			{
				printf("Size of join request not correct \n");
				result= false;
				break;
			}
			result= joinReceived((struct request_join*)pReq );
			break;
		case REQ_KEEP_ALIVE:
			if (sizeof(struct request_keep_alive) != len)
			{
				printf("Size of keep alive request not correct \n");
				result= false;
				break;
			}
			result= keepAliveReceived((struct request_keep_alive*)pReq);
			break;
		default:
			printf("Packet received but no recognized. \n");
			break;
	}

	return result;
}

/*******************************************************************************
 Function:    loginReceived

 Description: Login request received (This function is just for consistency

 Parameters:  pReq - the login request

 Returned:    true
*******************************************************************************/
int loginReceived(struct request_login *pReq)
{
	int result;
	result = addNewUser(pReq->req_username);
	if (result == true)
	{
		gettimeofday(&begin , NULL);
		mUserAliveCheck.insert(pair<string,long>(pReq->req_username, begin.tv_sec));
	}
	return result;
}

/*******************************************************************************
 Function:    removeRecentUser

 Description: Removes the user from the server

 Parameters:  None

 Returned:    true
*******************************************************************************/
int removeRecentUser()
{
	string strAdr = strAddressString((struct sockaddr_in*)&mostRecentUser);
	string checkForUser = mAddress2User[strAdr];

	if (checkForUser == "")
	{
		printf("No record of user to remove. \n");
		return false;
	}

	mAddress2User.erase(strAdr);
	mUser2Address.erase(checkForUser);
	mUserAliveCheck.erase(checkForUser);

	printf("%s %s recv Request Logout for user %s \n", strIPHost.c_str(), strAdr.c_str(), checkForUser.c_str());

	return true;
}

/*******************************************************************************
 Function:    logoutReceived

 Description: User requests for logout, function removes the user

 Parameters:  pReq - the login request

 Returned:    true if successfully logs out user else false for error
*******************************************************************************/
int logoutReceived(struct request_logout *req)
{
	int result;
	string strUser = strAddressUser ((struct sockaddr_in*)&mostRecentUser);
	pair<multimap<string,string>::iterator,
	multimap<string,string>::iterator> pairIteratorCheck;
	multimap<string,string>::iterator iter;

	if (req == NULL)
	{
		return false;
	}

	result = false;
	pairIteratorCheck = mUser2Chan.equal_range(strUser);

	char *pUsr = (char*) malloc(sizeof(char) * BUFFSIZE);
	char *pChan = (char*) malloc(sizeof(char) * BUFFSIZE);
	strcpy(pUsr, strUser.c_str());

	for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second; ++iter)
	{
		strcpy(pChan, iter->second.c_str());
		if (pChan == NULL)
		{
			return false;
		}
		result= eraseFromChan(pUsr, pChan);
	}
	result = removeRecentUser();

	free(pUsr);
	free(pChan);
	return result;
}

/*******************************************************************************
 Function:    listReceived

 Description: User requests for list of channels (This is just for consistency)

 Parameters:  pReq - the login request

 Returned:    true if successfully list channels for user else false for error
*******************************************************************************/
int listReceived(struct request_list *pReq)
{
	if (pReq == NULL)
	{
		return false;
	}

	return listChannels();
}

/*******************************************************************************
 Function:    listChannels

 Description: List all the channels

 Parameters:  None

 Returned:    true if successfully list channels for user else false for error
*******************************************************************************/
int listChannels()
{
	string sUsr = strAddressUser((struct sockaddr_in*)&mostRecentUser);
	set<string> sChan;
	multimap<string,string>::iterator iter;
	set<string,string>::iterator iterTwo;
	string recentAdr = strAddressString((struct sockaddr_in*)&mostRecentUser);
	char *strUserCon = (char*) malloc(sizeof(char) * BUFFSIZE);
	strcpy(strUserCon, sUsr.c_str());

	int i = 0;

	for (iter = mChan2User.begin(); iter != mChan2User.end(); iter++)
	{
		sChan.insert(iter->first);
	}

	struct text_list *lstChannels = (struct text_list*)
			malloc(sizeof(struct text_list) +
					sChan.size()*sizeof(struct channel_info));
	lstChannels->txt_type= ((int)TXT_LIST);
	lstChannels->txt_nchannels= ((int)sChan.size());

	for (iterTwo = sChan.begin(); iterTwo != sChan.end(); iterTwo++)
	{
		strncpy(lstChannels->txt_channels[i++].ch_channel, iterTwo->c_str(),
				CHANNEL_MAX);
	}

	int result = sendLast((struct text*)lstChannels, sizeof(struct text_list) + sChan.size()*sizeof(struct channel_info));

	if (result == true)
	{
		printf("%s %s recv Request List \n" ,strIPHost.c_str(), recentAdr.c_str());
	}

	mUserAliveCheck.erase(strUserCon);
	gettimeofday(&begin , NULL);
	mUserAliveCheck.insert(pair<string,long>(strUserCon, begin.tv_sec));
	free(lstChannels);
	return true;
}

/*******************************************************************************
 Function:    s2sSendLast

 Description: Handles s2s Send request received

 Parameters:  newMessage - The message to be sent
 	 	 	  length	 - The length of the message

 Returned:    true if successfully sends message else false for error
*******************************************************************************/
int s2sSendLast (struct request *newMessage, int length)
{
	string sNewAddr;
	struct sockaddr_in *pSockAddr= (struct sockaddr_in*)&mostRecentUser;

	sNewAddr = strAddressString((struct sockaddr_in*)&mostRecentUser);
	pSockAddr->sin_port= htons(pSockAddr->sin_port);

	return s2sSend((struct sockaddr*)&mostRecentUser, recentUserSize,
					newMessage, length);
}

/*******************************************************************************
 Function:    sendLast

 Description: Sends message to last user

 Parameters:  newMessage - The message to be sent
 	 	 	  length	 - The length of the message

 Returned:    true if successfully sends message else false for error
*******************************************************************************/
int sendLast(struct text *newMessage, int lenMsg)
{
	return sendMessage((struct sockaddr*)&mostRecentUser, recentUserSize,
						newMessage, lenMsg);
}

/*******************************************************************************
 Function:    serv2Chan

 Description: Add channel to server pair to multimap

 Parameters:  sAdrs - The address to add
 	 	 	  chan	- The channel to add

 Returned:    true if successfully adds pair to map false if error occured
*******************************************************************************/
int serv2Chan(string sAdrs, const char* chan)
{
	int result;
	string channel;

	result = chanDuplicatedCheck(sAdrs, chan);
	if (result == true)
	{
		channel = chan;
		mChan2Server.insert(pair<string, string>(channel, sAdrs));
	}

	return result;
}

/*******************************************************************************
 Function:    s2sJoinChan

 Description: Handles s2s_join request

 Parameters:  channel - The channel to be joined

 Returned:    true if successfully joins false if error
*******************************************************************************/
int s2sJoinChan(const char *channel)
{
	int req;
	int result;
	int vecTrack;

	string sAddr = strAddressString((struct sockaddr_in*) &mostRecentUser);
	struct request_s2s_join *pReq = (struct request_s2s_join*) malloc(sizeof(struct request_s2s_join));
	req = REQ_S2S_JOIN;
	pReq->req_type= req;
	strncpy(pReq->req_channel, channel, CHANNEL_MAX);

	result = s2sRelay ((struct request*)pReq, sizeof(struct request_s2s_join),
					  channel);

	for (vecTrack = 0; vecTrack < vAdressServer.size(); vecTrack++)
	{
		serv2Chan(vAdressServer[vecTrack].first, channel);
	}

	free(pReq);
	return result;
}

/*******************************************************************************
 Function:    chanDuplicatedCheck

 Description: Checks to see if channel is a duplicate

 Parameters:  sAd     - The address to check
 	 	 	  channel - The channel to check

 Returned:    true if not a duplicate else false
*******************************************************************************/
bool chanDuplicatedCheck (string sAdr, const char *channel)
{
	multimap<string,string>::iterator iter;
	string convertedStrChan = channel;
	pair<multimap<string,string>::iterator,
	     multimap<string,string>::iterator> pairIteratorCheck;
	int i;
	int result = true;

	pairIteratorCheck = mChan2Server.equal_range(convertedStrChan);

	for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second;
																		 iter++)
	{
		if (iter->first == convertedStrChan && iter->second == sAdr)
		{
			result = false;
			return result;
		}
	}
	return result;
}

/*******************************************************************************
 Function:    addNewUser

 Description: Adds user to server after login requested

 Parameters:  user    - The username of the client logging in

 Returned:    true if logging in is successful else false because of error
*******************************************************************************/
int addNewUser(const char* user)
{
	string cUserToStr;
	string recentAddress;
	string lookupAddress;

	if (user == NULL)
	{
		printf("No username, given null\n");
		return false;
	}

	cUserToStr = user;
	recentAddress = strAddressString((struct sockaddr_in*)&mostRecentUser);
	lookupAddress = mUser2Address[cUserToStr];

	if (recentAddress != lookupAddress && lookupAddress != "")
	{
		printf("%s %s recv Request Login with username %s; replacing %s with %s\n",
				strIPHost.c_str(), recentAddress.c_str(), user, lookupAddress.c_str(), recentAddress.c_str());
		mAddress2User.erase(lookupAddress);
	} else
	{
		printf("%s %s recv Request Login with username %s\n", strIPHost.c_str(), recentAddress.c_str(), user);
	}

	mAddress2User[recentAddress]= cUserToStr;
	mUser2Address[cUserToStr]= recentAddress;

	return true;
}

/*******************************************************************************
 Function:    s2sRelay

 Description: Handles s2s_forward request

 Parameters:  newMessage - The request type
 	 	 	  length     - Length of the request
 	 	 	  channel    - The channel to send it to

 Returned:    true if sent successfully else false because of error
*******************************************************************************/
int s2sRelay(struct request *newMessage, int length, string channel)
{
	int i;
	int result = false;
	struct sockaddr_in* sAdr= (struct sockaddr_in*) &mostRecentUser;
	sAdr->sin_port= ntohs(sAdr->sin_port);
	string recentAdr = strAddressString((struct sockaddr_in*) &mostRecentUser);

	if (vAdressServer.size() == 0)
	{
		return result;
	}

	for (i= 0; i < vAdressServer.size(); i++)
	{
		result = true;
		const struct sockaddr * sockCorrectAddress = sStringAddress(vAdressServer[i].first);
		result = s2sSend(sockCorrectAddress, sizeof(struct sockaddr_in), newMessage, length) && result;
		printf("%s %s send S2S Join %s \n", strIPHost.c_str(), vAdressServer[i].first.c_str(), channel.c_str());

	}
	return result;
}

/*******************************************************************************
 Function:    s2sSend

 Description: Sends new request

 Parameters:  sAdr      - The address to send to
 	 	 	  adrSize   - Size of address
 	 	 	  psRequest - The type of request to send

 Returned:    true if sent successfully else false because of error
*******************************************************************************/
int s2sSend(const struct sockaddr *sAdr, size_t adrSize,
			struct request *psRequest, int mesgSize)
{
	int result;
	result = sendto(sockUDP, psRequest, mesgSize, 0, sAdr, adrSize);
	if (result == -1)
	{
		perror("Error resulted when sending. \n");
		return false;
	}
	return true;
}


/*******************************************************************************
 Function:    say

 Description: Sends new message to users on channel

 Parameters:  chan       - The channel to send to
 	 	 	  cUser      - The user that sent it
 	 	 	  newMessage - The message text
 	 	 	  sockAdr    - The socket address to send to

 Returned:    true if sent successfully else false because of error
*******************************************************************************/
int say(const char *chan, const char *cUser, const char *newMessage,
		const struct sockaddr *sockAdr)
{
	int result;
	struct text_say *tSayText= (struct text_say*) malloc(sizeof(struct text_say));

	string addrStr= strAddressString((struct sockaddr_in*)&mostRecentUser);

	strncpy(tSayText->txt_text, newMessage, SAY_MAX);
	strncpy(tSayText->txt_channel, chan, CHANNEL_MAX);
	tSayText->txt_type=  (int)TXT_SAY;
	strncpy(tSayText->txt_username, cUser, USERNAME_MAX);

	result = sendMessage(sockAdr, sizeof(struct sockaddr_in),
			(struct text*)tSayText, sizeof(struct text_say));

	free(tSayText);

	map<string,long>::iterator itertwo;

	for (itertwo = mUserAliveCheck.begin(); itertwo != mUserAliveCheck.end(); itertwo++)
	{
		string test = cUser;
		if (test == itertwo->first.c_str())
		{
			gettimeofday(&begin , NULL);
			mUserAliveCheck.insert(pair<string,long>(cUser, begin.tv_sec));
		}
	}

	return result;
}


/*******************************************************************************
 Function:    eraseFromChan

 Description: Removes user from the specified channel

 Parameters:  user - The username of the user that wants to be removed
 	 	 	  chan - The channel they want to be removed from

 Returned:    true if sent successfully else false because of error
*******************************************************************************/
int eraseFromChan(const char *user, const char *chan)
{
	bool bFound = false;
	string recentAdr = strAddressString((struct sockaddr_in*)&mostRecentUser);
	string sUsr = user;
	string sChan = chan;
	multimap<string,string>::iterator iter;
	pair<multimap<string,string>::iterator,
	multimap<string,string>::iterator> pairIteratorCheck;

	if (strlen(chan) > CHANNEL_MAX || chan == NULL )
	{
		printf("Server: %s trying to leave null channel %s\n", user, chan);
		mUserAliveCheck.erase(user);
		gettimeofday(&begin , NULL);
		mUserAliveCheck.insert(pair<string,long>(user, begin.tv_sec));
		return false;
	}
	else if (user == NULL)
	{
		printf("Can not remove, no username provided. \n");
		mUserAliveCheck.erase(user);
		gettimeofday(&begin , NULL);
		mUserAliveCheck.insert(pair<string,long>(user, begin.tv_sec));
		return false;
	}

	pairIteratorCheck = mUser2Chan.equal_range(sUsr);
	for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second; iter++)
	{
		if (iter->second == sChan)
		{
			bFound = true;
			mUser2Chan.erase(iter);
			break;
		}
	}
	if (bFound == false )
	{
		printf("%s %s server: %s trying to leave non-existent channel %s\n", strIPHost.c_str(), recentAdr.c_str(), user, chan);
		mUserAliveCheck.erase(user);
		gettimeofday(&begin , NULL);
		mUserAliveCheck.insert(pair<string,long>(user, begin.tv_sec));
		return false;
	}

	pairIteratorCheck = mChan2User.equal_range(sChan);
	bFound = false;

	for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second; ++iter)
	{
		if (iter->second == sUsr)
		{
			bFound = true;
			mChan2User.erase(iter);
			printf("%s %s server: %s leaves channel %s\n", strIPHost.c_str(),
					recentAdr.c_str(), iter->second.c_str(),
					iter->first.c_str());
			mUserAliveCheck.erase(user);
			gettimeofday(&begin , NULL);
			mUserAliveCheck.insert(pair<string,long>(user, begin.tv_sec));
			break;
		}
	}
	if (!bFound)
	{
		printf("%s %s server: %s trying to leave channel %s "
				"where he/she is not a member \n", strIPHost.c_str(),
				recentAdr.c_str(), user, chan);
		mUserAliveCheck.erase(user);
		gettimeofday(&begin , NULL);
		mUserAliveCheck.insert(pair<string,long>(user, begin.tv_sec));
		return false;
	}

	return true;
}


/*******************************************************************************
 Function:    joinReceived

 Description: Handles Join request received from client to join a channel

 Parameters:  pRes - The request_join struct

 Returned:    true if sent successfully else false because of error
*******************************************************************************/
int joinReceived(struct request_join *pReq)
{
	int result;
	string channel = pReq->req_channel;
	string sUsr = strAddressUser((struct sockaddr_in*)&mostRecentUser);

	sChanSet.insert(channel);

	result = joinToChannel(sUsr.c_str(), pReq->req_channel);
	mUserAliveCheck.erase(sUsr.c_str());
	gettimeofday(&begin , NULL);
	mUserAliveCheck.insert(pair<string,long>(sUsr.c_str(), begin.tv_sec));
	return result;
}


/*******************************************************************************
 Function:    leaveReceived

 Description: Handles Leave request received from user

 Parameters:  pReq - The request_leave struct

 Returned:    true if sent successfully else false because of error
*******************************************************************************/
int leaveReceived(struct request_leave *pReq)
{
	string sUsr = strAddressUser((struct sockaddr_in*)&mostRecentUser);
	string rectentAdr = strAddressString((struct sockaddr_in*)&mostRecentUser);
	char *userConvertS = (char*) malloc(sizeof(char) * BUFFSIZE);

	strcpy(userConvertS, sUsr.c_str());

	printf("%s %s recv Request Leave for User %s from Channel %s \n", strIPHost.c_str(), rectentAdr.c_str(), userConvertS, pReq->req_channel);

	int result = eraseFromChan(userConvertS, pReq->req_channel);

	mUserAliveCheck.erase(userConvertS);
	gettimeofday(&begin , NULL);
	mUserAliveCheck.insert(pair<string,long>(userConvertS, begin.tv_sec));
	free(userConvertS);
	return result;
}

/*******************************************************************************
 Function:    eraseChanFromSer

 Description: Removes channel from server

 Parameters:  sAdr    - The address to remove
 	 	 	  channel - The channel to remove

 Returned:    true if removed successfully else false because of error
*******************************************************************************/
int eraseChanFromSer(string sAdr, char *channel)
{
	string sChan = channel;
	multimap<string,string>::iterator iter;
	pair<multimap<string,string>::iterator,
	multimap<string,string>::iterator> pairIteratorCheck;

	pairIteratorCheck= mChan2Server.equal_range(sChan);
	for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second; iter++)
	{
		if (iter->first == sChan && iter->second == sAdr)
		{
			mChan2Server.erase(iter);
			return true;
		}
	}
	return false;
}

/*******************************************************************************
 Function:    s2sLeaveReceived

 Description: Handles request_s2s_leave request struct

 Parameters:  pReq - The request_s2s_leave struct

 Returned:    true if removed successfully else false because of error
*******************************************************************************/
int s2sLeaveReceived(struct request_s2s_leave *pReq)
{
	int result = true;
	string address_string;
	string user_string;
	if (pReq == NULL)
	{
		return false;
	}

	struct sockaddr_in* sin = (struct sockaddr_in*) &mostRecentUser;
	sin->sin_port= ntohs(sin->sin_port);

	address_string = strAddressString(sin);
	user_string = strAddressUser((struct sockaddr_in*)&mostRecentUser);

	printf("%s %s recv S2S leave channel %s\n", strIPHost.c_str(),
			address_string.c_str(), pReq->req_channel);

	result = eraseChanFromSer(address_string, pReq->req_channel);

	return result;
}

/*******************************************************************************
 Function:    joinToChannel

 Description: Joins a user to the requested channel

 Parameters:  user - The username of the client requesting to join the channel
 	 	 	  chan - The name of the channel they wish to join

 Returned:    true if joined successfully else false because of error
*******************************************************************************/
int joinToChannel(const char *user, const char *chan)
{
	string sUsr = user;
	string sChan = chan;
	string recentAdr = strAddressString((struct sockaddr_in*)&mostRecentUser);
	multimap<string,string>::iterator iter;
	pair<multimap<string,string>::iterator,
		 multimap<string,string>::iterator> pairIteratorCheck;
	bool bFound = false;

	if (chan == NULL )
	{
		printf("server: can not add user to null channel \n");
		mUserAliveCheck.erase(user);
		gettimeofday(&begin , NULL);
		mUserAliveCheck.insert(pair<string,long>(user, begin.tv_sec));
		return false;
	} else if (user == NULL)
	{
		printf("server: can not add null username to channel \n");
		mUserAliveCheck.erase(user);
		gettimeofday(&begin , NULL);
		mUserAliveCheck.insert(pair<string,long>(user, begin.tv_sec));
		return false;
	}

	pairIteratorCheck= mUser2Chan.equal_range(sUsr);
	for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second; iter++)
	{
		if (iter->second == sChan)
		{
			bFound= true;
			printf("%s %s recv Request Join %s for User %s; User already belongs"
					" to channel \n",strIPHost.c_str(), recentAdr.c_str(), chan, user);
			mUserAliveCheck.erase(user);
			gettimeofday(&begin , NULL);
			mUserAliveCheck.insert(pair<string,long>(user, begin.tv_sec));
			return false;
		}
	}
	if (!bFound)
	{
		mUser2Chan.insert(pair<string,string>(sUsr, sChan));
		printf("%s %s recv Request Join %s for User %s \n", strIPHost.c_str(),
				recentAdr.c_str(), chan, user);
		mUserAliveCheck.erase(user);
		gettimeofday(&begin , NULL);
		mUserAliveCheck.insert(pair<string,long>(user, begin.tv_sec));
		s2sJoinChan(chan);
	}

	bFound = false;
	pairIteratorCheck= mChan2User.equal_range(sChan);
	for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second; iter++)
	{
		if (iter->second == sUsr) {
			bFound = true;
			break;
		}
	}
	if (bFound == false)
	{
		mChan2User.insert(pair<string,string>(sChan, sUsr));
	}

	return true;
}

/*******************************************************************************
 Function:    sendMessage

 Description: Sends message to specified socket address

 Parameters:  sAdr    - The socket Address
 	 	 	  lensAdr - The length of the socket address
 	 	 	  text 	  - The struct containing the message
 	 	 	  lenMsg  - The length of the message

 Returned:    true if sent successfully else false because of error
*******************************************************************************/
int sendMessage(const struct sockaddr *sAdr, size_t lensAdr,
				struct text *newMessage, int lenMsg)
{
	int result;

	result = sendto(sockUDP, newMessage, lenMsg, 0, sAdr, lensAdr);
	if (result == -1)
	{
		perror("Error sending message \n");
		return false;
	}
	return true;
}

/*******************************************************************************
 Function:    s2sSayNewID

 Description: Handles s2s_say with new ID assigned

 Parameters:  newMessage - The message being sent
 	 	 	  usr        - The username
 	 	 	  chan 	     - The channel name

 Returned:    true if say is sent successfully else false because of error
*******************************************************************************/
int s2sSayNewID (char *newMessage, char *usr, char *chan)
{
	int result;
	if (newMessage == NULL || usr == NULL || chan == NULL)
	{
		return false;
	}

	struct request_s2s_say *pReq = (struct request_s2s_say*)
			malloc(sizeof(struct request_s2s_say));

	pReq->req_type = ((int)REQ_S2S_SAY);

	strncpy(pReq->req_text, newMessage, SAY_MAX);
	strncpy(pReq->req_username, usr, USERNAME_MAX);
	strncpy(pReq->req_channel, chan, CHANNEL_MAX);

	pReq->uid= IDgenerator();

	result = s2sSay(pReq);
	free(pReq);

	return result;
}

/*******************************************************************************
 Function:    s2sJoinReceived

 Description: Handles request_s2s_join request

 Parameters:  pReq - The struct request_s2s_join

 Returned:    true if joined successfully else false because of error
*******************************************************************************/
int s2sJoinReceived(request_s2s_join* pReq)
{
	string chan = pReq->req_channel;
	struct sockaddr_in *psAdress = (struct sockaddr_in*) &mostRecentUser;
	psAdress->sin_port = ntohs(psAdress->sin_port);
	string sAdrOnly = strAddressString((struct sockaddr_in*) &mostRecentUser);
	psAdress->sin_port= htons(psAdress->sin_port);

	serv2Chan(sAdrOnly, pReq->req_channel);

	vector<pair<string,long> >::iterator it;
	for(it = vAdressServer.begin(); it != vAdressServer.end(); ++it)
	{
	    if (it->first == sAdrOnly)
	    {
	    	gettimeofday(&begin , NULL);
	    	vAdressServer.erase(it);
	    	vAdressServer.push_back(pair<string,long>(sAdrOnly, begin.tv_sec));
	    }
	}

	printf("%s %s recv S2S Join %s \n", strIPHost.c_str(), sAdrOnly.c_str(), chan.c_str());

	if (sChanSet.find(chan) == sChanSet.end())
	{
		sChanSet.insert(chan);
		return s2sJoinChan(pReq->req_channel);
	}
	return false;
}

/*******************************************************************************
 Function:    whoReceived

 Description: Handles request_who

 Parameters:  pReq - The struct: request_who

 Returned:    true if who command is successful else false because of error
*******************************************************************************/
int whoReceived (struct request_who* pReq)
{

	return whoIsOnThisChannel(pReq->req_channel);
}

/*******************************************************************************
 Function:    whoIsOnThisChannel

 Description: List the user on the specificed channel

 Parameters:  chan - The channel being requested to list the users on

 Returned:    true if list is successful else false because of error
*******************************************************************************/
int whoIsOnThisChannel(const char *chan)
{
	string sChan = chan;
	string sUser;
	string recentAdr = strAddressString((struct sockaddr_in*)&mostRecentUser);
	string strUser = strAddressUser ((struct sockaddr_in*)&mostRecentUser);
	multimap<string,string>::iterator iter;
	pair<multimap<string,string>::iterator,
	multimap<string,string>::iterator> pairIteratorCheck;
	int usersOnline = 0;
	char *userConvertS = (char*) malloc(sizeof(char) * BUFFSIZE);
	int txtWho = TXT_WHO;

	strcpy(userConvertS, strUser.c_str());

	if (chan == NULL)
	{
		printf("No channel to look up as it was null \n");
		return false;
	}


	pairIteratorCheck = mChan2User.equal_range(sChan);

	for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second; iter++)
	{
		usersOnline++;
	}

	if (usersOnline == 0)
	{
		printf("%s %s recv Request Who, Channel %s does not exist \n" , strIPHost.c_str(), recentAdr.c_str(), chan);
		mUserAliveCheck.erase(userConvertS);
		gettimeofday(&begin , NULL);
		mUserAliveCheck.insert(pair<string,long>(userConvertS, begin.tv_sec));
		return false;
	}

	struct text_who *lstWhosOn= (struct text_who*) malloc(sizeof(struct text_who)
								+ usersOnline*sizeof(struct user_info));

	lstWhosOn->txt_type= (txtWho);
	lstWhosOn->txt_nusernames= (usersOnline);

	strncpy(lstWhosOn->txt_channel, chan, CHANNEL_MAX);

	int i = 0;
	pairIteratorCheck = mChan2User.equal_range(sChan);

	for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second; iter++)
	{
		strncpy(lstWhosOn->txt_users[i++].us_username, iter->second.c_str(),
				USERNAME_MAX);
	}

	int result= sendLast((struct text*)lstWhosOn, sizeof(struct text_who) +
			usersOnline*sizeof(struct user_info));
	if (result == true)
	{
		printf("%s %s recv Request Who \n", strIPHost.c_str(), recentAdr.c_str());
	}

	mUserAliveCheck.erase(userConvertS);
	gettimeofday(&begin , NULL);
	mUserAliveCheck.insert(pair<string,long>(userConvertS, begin.tv_sec));
	free(lstWhosOn);

	return result;
}

/*******************************************************************************
 Function:    s2sSayReceived

 Description: Handles request_s2s_say

 Parameters:  pReq - The struct containg the request: request_s2s_say

 Returned:    true if sayReceived is successful else false because of error
*******************************************************************************/
int s2sSayReceived(struct request_s2s_say *pReq)
{
	struct request_say* psRequest= (struct request_say*)
			malloc(sizeof(struct request_say));
	string recentAdress = strAddressString((struct sockaddr_in*)&mostRecentUser);

	strncpy(psRequest->req_channel, pReq->req_channel, CHANNEL_MAX);
	strncpy(psRequest->req_text, pReq->req_text, SAY_MAX);
	int result;

	printf("%s %s recv S2S Say %s %s '%s' \n", strIPHost.c_str(), recentAdress.c_str(),
			pReq->req_username, pReq->req_channel, pReq->req_text);

	if(sIDList.find(pReq->uid) == sIDList.end())
	{
		result = sayReceived(psRequest, pReq->uid, pReq->req_username);

		if (result == false)
		{
			printf("%s %s Error forwarding message, sending leave\n", strIPHost.c_str(), recentAdress.c_str());
			s2sLeaveChan(pReq->req_channel);
		}
		free(psRequest);
		return result;
	}

	printf("%s %s Same ID found multiple times. Leaving.\n", strIPHost.c_str(), recentAdress.c_str());
	struct sockaddr_in *newSin = (struct sockaddr_in*) &mostRecentUser;
	newSin ->sin_port= ntohs(newSin ->sin_port);
	s2sLeaveChan(pReq->req_channel);
	return false;
}

/*******************************************************************************
 Function:    Handles s2s Leave channel request

 Description: Allows user wants to leave the specified channel

 Parameters:  chan - The channel being left

 Returned:    true if list is successful else false because of error
*******************************************************************************/
int s2sLeaveChan(const char *chan)
{
	string recentAddress;
	int result;
	if (chan == NULL)
	{
		return false;
	}

	struct request_s2s_leave *pReq = (struct request_s2s_leave*)
			malloc(sizeof(struct request_s2s_leave));
	recentAddress = strAddressString((struct sockaddr_in*) &mostRecentUser);

	pReq->req_type= ((int)REQ_S2S_LEAVE);

	strncpy(pReq->req_channel, chan, CHANNEL_MAX);
	printf("%s %s send S2S leave \n", strIPHost.c_str(), recentAddress.c_str());

	result = s2sSendLast((struct request*) pReq,
			sizeof(struct request_s2s_leave));

	free(pReq);
	return result;
}

/*******************************************************************************
 Function:    s2sSay

 Description: Handles request: request_s2s_say

 Parameters:  pReq - The struct containing the request_s2s_say

 Returned:    true if say is successful else false because of error
*******************************************************************************/
int s2sSay(struct request_s2s_say* pReq)
{
	int result = false;
	string recentAdr;
	pair<std::multimap<std::string,std::string>::iterator,
	     std::multimap<std::string,std::string>::iterator> pairIteratorCheck;
	multimap<std::string,std::string>::iterator iter;
	string adrLookUp;
	struct sockaddr *sNewSockAdr;

	if (pReq == NULL)
	{
		return false;
	}

	sIDList.insert(pReq->uid);

	char *chan = (char*) malloc(sizeof(char) * BUFFSIZE);
	sprintf(chan, "%s", pReq->req_channel);
	string sChan = chan;

	struct sockaddr_in *sockAdr= (sockaddr_in*) &mostRecentUser;
	sockAdr->sin_port= ntohs(sockAdr->sin_port);
	recentAdr = strAddressString(sockAdr);
	adrLookUp = strAddressString((struct sockaddr_in*)&mostRecentUser);

	pairIteratorCheck = mChan2Server.equal_range(sChan);
	for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second;
			iter++)
	{
		if (iter->first == sChan && iter->second != recentAdr)
		{
			struct sockaddr *sNewSockAdr = sStringAddress(iter->second);
			printf("%s %s send S2S Say %s %s '%s' \n", strIPHost.c_str(),
					adrLookUp.c_str(), pReq->req_username, pReq->req_channel, pReq->req_text);
			result = s2sSend(sNewSockAdr, sizeof(struct sockaddr_in),
					(struct request *)pReq, sizeof(struct request_s2s_say)) || result;

		}
	}
	return result;
}

/*******************************************************************************
 Function:    Handles say request when received

 Description: Sends message from user

 Parameters:  pReq   - The struct contain the request_say
 	 	      UserID - The unique ID of the user trying to send a message
 	 	      pUsr   - The username of the user trying to send a message

 Returned:    true if sending the msg is successful else false because of error
*******************************************************************************/
int sayReceived(struct request_say *pReq, long long UserID, char *pUsr)
{
	int result= false;

	if (pUsr == NULL)
	{
		string userSrc = strAddressUser((struct sockaddr_in*)&mostRecentUser);
		pUsr = (char*) malloc(sizeof(char) * BUFFSIZE);
		strncpy(pUsr, userSrc.c_str(), USERNAME_MAX);
	}

	pair<multimap<string,string>::iterator,
		multimap<string,string>::iterator> pairIteratorCheck;
	multimap<string,string>::iterator iter;
	string channelRequest = pReq->req_channel;
	pairIteratorCheck = mChan2User.equal_range(channelRequest);
	//bool print = false;

	for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second;
			iter++)
	{
		string sUser = iter->second;
		string sAddrUser = mUser2Address[sUser];

		char *cToken= (char*) malloc(sizeof(char)*BUFFSIZE);
		strcpy(cToken, sAddrUser.c_str());

		char *ip = strtok(cToken, ":");
		char *port = strtok(NULL, ":");
		u_short pShort = (u_short) atoi(port);

		struct sockaddr_in sockAdr;
		inet_pton(AF_INET, ip, &(sockAdr.sin_addr));
		sockAdr.sin_port = pShort;
		sockAdr.sin_family = AF_INET;

		string recentAdr = strAddressString((struct sockaddr_in*)&mostRecentUser);
		string usrSrcAdv = strAddressUser((struct sockaddr_in*)&mostRecentUser);

		printf("%s %s recv Request Say %s '%s'\n", strIPHost.c_str(), recentAdr.c_str(), pReq->req_channel, pReq->req_text);

		say(pReq->req_channel, pUsr, pReq->req_text, (struct sockaddr*)&sockAdr);

		/*mUserAliveCheck.erase(pUsr);
		gettimeofday(&begin , NULL);
		mUserAliveCheck.insert(pair<string,long>(pUsr, begin.tv_sec));*/
		result= true;
		free(cToken);
	}

	if (UserID == 0)
	{
		result = s2sSayNewID (pReq->req_text, pUsr, pReq->req_channel) || result;
	}
	else
	{
		struct request_s2s_say* s2sSayReq= (struct request_s2s_say*)
				malloc(sizeof(struct request_s2s_say));

		strncpy(s2sSayReq->req_username, pUsr, USERNAME_MAX);
		strncpy(s2sSayReq->req_channel, pReq->req_channel, CHANNEL_MAX);
		strncpy(s2sSayReq->req_text, pReq->req_text, SAY_MAX);

		s2sSayReq->uid = UserID;
		s2sSayReq->req_type = ((int)REQ_S2S_SAY);

		result = s2sSay(s2sSayReq) || result;
		free(s2sSayReq);
	}

	return result;
}

/*******************************************************************************
 Function:    keepAliveReceived

 Description: Handles keep alive request

 Parameters:  pReq - The struct containing the request_keep_alive

 Returned:    true if sending the keep_alive is successful else false because of
              error
*******************************************************************************/
int keepAliveReceived(struct request_keep_alive *pReq)
{
	string strUser = strAddressUser ((struct sockaddr_in*)&mostRecentUser);
	char *userConvertS = (char*) malloc(sizeof(char) * BUFFSIZE);
	strcpy(userConvertS, strUser.c_str());

	if (pReq == NULL)
	{
		return false;
	}

	printf("%s server: %s keeps alive\n" , strIPHost.c_str(), userConvertS);
	mUserAliveCheck.erase(userConvertS);
	gettimeofday(&begin , NULL);
	mUserAliveCheck.insert(pair<string,long>(userConvertS, begin.tv_sec));
	return true;
}

/*******************************************************************************
 Function:    kickUserOff

 Description: User has not sent anything in two minutes so kicking them off

 Parameters:  user - the user to remove
 	 	      chan - the channel to remove from

 Returned:    true if kick off is successful else false for error
*******************************************************************************/
int kickUserOff (const char* user, const char* chan)
{
	int result;
	pair<multimap<string,string>::iterator,
	multimap<string,string>::iterator> pairIteratorCheck;
	multimap<string,string>::iterator iter;

	if (user == NULL || chan == NULL)
	{
		return false;
	}

	result = false;
	pairIteratorCheck = mUser2Chan.equal_range(user);

	for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second; ++iter)
	{
		if (chan == NULL)
		{
			return false;
		}
		bool bFound = false;
		multimap<string,string>::iterator iter;
		pair<multimap<string,string>::iterator,
		multimap<string,string>::iterator> pairIteratorCheck;

		if (strlen(chan) > CHANNEL_MAX || chan == NULL )
		{
			printf("Server: %s trying to leave null channel %s\n", user, chan);
			mUserAliveCheck.erase(user);
			gettimeofday(&begin , NULL);
			mUserAliveCheck.insert(pair<string,long>(user, begin.tv_sec));
			return false;
		}
		else if (user == NULL)
		{
			printf("Can not remove, no username provided. \n");
			mUserAliveCheck.erase(user);
			gettimeofday(&begin , NULL);
			mUserAliveCheck.insert(pair<string,long>(user, begin.tv_sec));
			return false;
		}

		pairIteratorCheck = mUser2Chan.equal_range(user);
		for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second; iter++)
		{
			if (iter->second == chan)
			{
				bFound = true;
				mUser2Chan.erase(iter);
				break;
			}
		}
		if (bFound == false )
		{
			printf("server: %s trying to leave non-existent channel %s\n", user, chan);
			mUserAliveCheck.erase(user);
			gettimeofday(&begin , NULL);
			mUserAliveCheck.insert(pair<string,long>(user, begin.tv_sec));
			return false;
		}

		pairIteratorCheck = mChan2User.equal_range(chan);
		bFound = false;

		for (iter = pairIteratorCheck.first; iter != pairIteratorCheck.second; ++iter)
		{
			if (iter->second == user)
			{
				bFound = true;
				mChan2User.erase(iter);
				break;
			}
		}
		if (!bFound)
		{
			printf("server: %s trying to leave channel %s "
					"where he/she is not a member \n", user, chan);
			mUserAliveCheck.erase(user);
			gettimeofday(&begin , NULL);
			mUserAliveCheck.insert(pair<string,long>(user, begin.tv_sec));
			return false;
		}
	}
	if (user == "")
	{
		printf("No record of user to remove. \n");
		return false;
	}

	mAddress2User.erase(mUser2Address.at(user));
	mUser2Address.erase(user);
	mUserAliveCheck.erase(user);

	return result;
}

/*******************************************************************************
 Function:    sendOutJoins

 Description: Kicks user off if they have not sent anything for two minutes

 Parameters:  time - the time

 Returned:    None
*******************************************************************************/
void sendOutJoins(int time)
{
	set<string>::const_iterator iter;
	string channel;
	if(!sChanSet.empty())
	{
		for (iter = sChanSet.begin(); iter !=sChanSet.end(); ++iter)
		{
			struct request_s2s_join *req= (struct request_s2s_join*) malloc(sizeof(struct request_s2s_join));
			req->req_type= ((int)REQ_S2S_JOIN);

			if (vAdressServer.size() == 0)
			{
				timesUp(time);
				return;
			}
			else
			{
				for (int i= 0; i < vAdressServer.size(); i++)
				{
					s2sJoinChan(iter->c_str());
				}
			}
		}
	}
	timesUp(time);
}
